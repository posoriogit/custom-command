# Intruduction
this tutorial is to create a nodejs command and use office365 cli to export list into csv

# How to run from source
```
git clone this project
npm i
node main.js
```

# How to run it from npmjs
```
npm i -g @quantr/quantr-spo
quantr-spo export-list tiffany@quantr.hk
```
