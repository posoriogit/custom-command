const Json2csvParser = require("json2csv").Parser;
const fs = require("fs");

const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
  })

    readline.question(`List Title: `, (title) => {
      
      readline.question(`Site Url: `, (url) => {
        readline.question(`Output File Name(example.csv): `,(fname)=>{
            const exec = require('child_process').exec;
            var child = exec(`o365 spo listitem list --title "${title}" -u ${url} -f "*" -o json`, //-o json
        
            (error, stdout, stderr) => {
                const json2csvParser = new Json2csvParser({ header: true});
                const csv = json2csvParser.parse(JSON.parse(stdout));

                fs.writeFile(fname, csv, function(error) {
                if (error) throw error;
                });

                if (error !== null) {
                console.log(`exec error: ${error}`);
                }else{
                    console.log("")
                }

            })
            readline.close()
        });
      });
    });
  